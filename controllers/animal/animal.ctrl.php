<?php

class AnimalAnimalController extends Controller
{
    protected $view = 'animal/animal.tpl';

    protected $animal;
    protected $animalName;
    protected $finalAnimal;
    protected $animalURL_0;
    protected $animalURL_1;
    protected $animalURL_2;

    public function build(){

        $info = $this->getParams();

        if(!$info['error'] && !$info['final']) {

            // Agafem el numero de l'animal
            $number = $info['number'];

            $this->assign($this->finalAnimal,false);

            // Agafem la galeria
            $galleryModel = $this->getClass('GalleryGalleryModel');
            $gallerySize = $galleryModel->getSize($this->animal);
            $gallery = $galleryModel->getMeAnimal($this->animal);

            // Depenent dels casos mostrarem la galeria d'una manera o un altre
            // Si no esta buida
            if($number < $gallerySize){

                $this->assign($this->finalAnimal,false);
                $this->assign($this->animalName,$gallery[$number]['name']);

                // Si estem al principi
                if(!$number){

                    $this->assign($this->animalURL_0,$gallery[$gallerySize - 1]['URL']);
                    $this->assign($this->animalURL_1,$gallery[$number]['URL']);
                    // Hi ha més d'una foto
                    if($gallerySize > 1)
                        $this->assign($this->animalURL_2,$gallery[$number + 1]['URL']);

                    // Nomes hi ha una foto
                    else
                        $this->assign($this->animalURL_2,'');
                }

                else {

                    $this->assign($this->animalURL_0,$gallery[$number - 1]['URL']);
                    $this->assign($this->animalURL_1,$gallery[$number]['URL']);

                    // Estem al mitj
                    if($number < $gallerySize - 1)
                        // Normal
                        $this->assign($this->animalURL_2,$gallery[$number + 1]['URL']);

                    // Quasi al final
                    else
                        $this->assign($this->animalURL_2,'');
                }
            }

            else {
                $this->assign($this->finalAnimal,true);
                $this->assign($this->animalName,'');
            }

            $this->setLayout($this->view);
        }
    }


    public function loadModules() {
        $modules['marmot']  = 'AnimalMarmotController';
        $modules['monkey']  = 'AnimalMonkeyController';
        $modules['platypus']  = 'AnimalPlatypusController';
        return $modules;
    }
}




?>