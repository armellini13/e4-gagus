<?php

include_once(PATH_CONTROLLERS . 'animal/animal.ctrl.php');

class AnimalPlatypusController extends AnimalAnimalController
{
    protected $view = 'animal/platypus.tpl';

    protected $animal = 'platypus';
    protected $animalName = 'platypusName';
    protected $finalAnimal = 'finalPlatypus';
    protected $animalURL_0 = 'platypusURL_0';
    protected $animalURL_1 = 'platypusURL_1';
    protected $animalURL_2 = 'platypusURL_2';

    public function loadModules() {

    }

}


?>