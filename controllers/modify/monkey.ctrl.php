<?php

class ModifyMonkeyController extends Controller
{
    protected $view = 'modify/monkey.tpl';

    public function build( )
    {
        $monkeyModel = $this->getClass('GalleryGalleryModel');
        $monkey = $monkeyModel->getMeAnimal('monkey');

        $this->assign('monkey',$monkey);

        $this->setLayout($this->view);

    }

}


?>