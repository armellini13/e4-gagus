<?php

class ModifyPlatypusController extends Controller
{
    protected $view = 'modify/platypus.tpl';
    public function build( )
    {
        $platypusModel = $this->getClass('GalleryGalleryModel');
        $platypus = $platypusModel->getMeAnimal('platypus');

        $this->assign('platypus',$platypus);

        $this->setLayout($this->view);


    }
}


?>