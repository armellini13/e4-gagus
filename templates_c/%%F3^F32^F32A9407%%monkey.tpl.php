<?php /* Smarty version 2.6.14, created on 2014-03-29 00:44:22
         compiled from monkey/monkey.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<!-- Això és un comentari HTML -->

<div class="block">
    <div id="inner-block" align="center">
        <h2 id="monkeyNumber"><?php echo $this->_tpl_vars['monkeyName']; ?>
</h2>
        <img src="<?php echo $this->_tpl_vars['URL']; ?>
">
        <br><br>
    <?php if (! $this->_tpl_vars['empty']): ?>
        <?php if ($this->_tpl_vars['numero'] != 0): ?>
            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/monkey/<?php echo $this->_tpl_vars['anterior']; ?>
">
                Anterior
            </a>
        <?php else: ?>
            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/monkey/<?php echo $this->_tpl_vars['lastMonkey']; ?>
">
                Anterior
            </a>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['numero'] != "{".($this->_tpl_vars['lastMonkey'])): ?>
            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/monkey/<?php echo $this->_tpl_vars['seguent']; ?>
">
                Següent
            </a>
        <?php else: ?>
            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/monkey/0">
                Següent
            </a>
        <?php endif; ?>
    <?php else: ?>
        <p>Ho sentim però no tenim cap fotografia a la galeria. Pots pujar fotos a l'apartat d'Upload!</p>
    <?php endif; ?>
        <br><br>
    </div>
</div>

<div class="clear"></div>
<?php echo $this->_tpl_vars['modules']['footer']; ?>