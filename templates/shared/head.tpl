<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="title" content="" />
	<meta name="robots" content="all" />
	<meta name="expires" content="never" />
	<meta name="distribution" content="world" />
	<title>The Zoo Gallery</title>
	<link rel="stylesheet" href="{$url.global}/css/style.css">
</head>
<body id="home">

    <div id="menuWrapper">
        <div id='cssmenu'>
            <ul>
            {   if $link == "home" || $link == "default"}
                <li class='active'><a href='{$url.global}/home'><span>Home</span></a></li>
            {   else}
                <li><a href='{$url.global}/home'><span>Home</span></a></li>
            {   /if}
            {   if $link == "gallery"}
                <li class='active'><a href='{$url.global}/gallery'><span>Gallery</span></a></li>
            {   else}
                <li><a href='{$url.global}/gallery '><span>Gallery</span></a></li>
            {   /if}
            {   if $link == "modify"}
            <li class='active'><a href='{$url.global}/modify'><span>Modify</span></a></li>
            {   else}
            <li><a href='{$url.global}/modify '><span>Modify</span></a></li>
            {   /if}
            {   if $link == "upload"}
            <li class='active'><a href='{$url.global}/upload'><span>Upload</span></a></li>
            {   else}
            <li><a href='{$url.global}/upload'><span>Upload</span></a></li>
            {   /if}
            {   if $link == "about"}
                <li class='active'><a href='{$url.global}/about'><span>About</span></a></li>
            {   else}
                <li><a href='{$url.global}/about'><span>About</span></a></li>
            {   /if}
            </ul>
        </div>
    </div>
    <div id="wrapper">