{$modules.head}

<div class="block">
    <div id="inner-block" align="center">

        {$modules.monkey}

        {$modules.platypus}

        {$modules.marmot}

    </div>
</div>

<div class="clear"></div>

{$modules.footer}