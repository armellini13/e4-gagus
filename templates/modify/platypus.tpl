<h2 class="title">Modify Platypus</h2>
{   if isset($platypus)}
<ul class="list">
    {   section name=i loop=$platypus}
    <li class="list">
        {$platypus[i].name}
        <a href="{$url.global}/modify/delete/platypus/{$platypus[i].ID}">Borrar</a>
        <a href="{$url.global}/modify/change/platypus/{$platypus[i].ID}">Modificar</a>
    </li>
    {   /section}
</ul>
{   else}
<p>No hi han platypus</p>
{   /if}
<br><br>